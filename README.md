# Skip The Dishes Finder

This is my project to show my architecture design decisions and my customization of design and UX Experience!

> "I Really would like to thank you for the opportunity
> to participate in this process and know more about you 
> and about your country and place to work."

#### That's is a little video to explain my project, please give me a feedback [YouTube Video](https://youtu.be/TXx5HlEMmRg)


## Features

- [x] Get your location 
- [x] Implement Google Places and Maps
- [x] Show restaurants nearest you in a list
- [x] Show restaurants nearest you in a map
- [x] Search for an address with a custom view
- [x] Show restaurants nearest your chosen location
- [x] Show information by click in the pin on the map
- [x] Make routes using Apple Maps
- [x] Make routes Google Maps API
- [x] Show the rating of the restaurant

## To Do's
- [ ] Get Phone information from place
- [ ] Make calls for the restaurants
- [ ] Show if is open or closed
- [ ] Entire localized project
- [ ] Resolve visual bugs :(

Documentation
----
Used Adobe XD to make a prototype and planning what to do, this files are in documentation, folder: ***documentation/design/SkipTheDishesRestaurantSearch.xd***

Files Tree 
----
Explained

| Folders | meaning |
| ------ | ------ |
| StartUp | Created to concentrate the files to start the build like info.plist and AppDelegate |
| Sections | Created to put the StoryBoards and split files by features |
| Domain | Created to handle with the business so if you have something like GooglePlaces API you have to concentrate ever feature that you need from in just one place |
| Support | Everything that you need to create and reuse for more than one place need to be here, like utils for Strings and UI Customizations like ColorDefaults |



### Tech

I use just few different things to help me with the development

* [Cocoapods](http://cocoapods.org) - is a dependency manager for Cocoa projects
* [Alamofire](https://github.com/Alamofire/Alamofire) - we need to make HTTP calls and this is the most important lib to do that on iOS
* [Lottie](https://github.com/airbnb/lottie-ios) - is an awesome tool to make animations and beauty things like loadings
* [Google Place](https://developers.google.com/places/ios-sdk/) - used to find the places on google maps
* [Google Maps](https://developers.google.com/maps/documentation/ios-sdk/) - used to find the places on google maps


### Installation

Skip The Dishes Finder requires Xcode 9.4+ and Cocoapods

### CocoaPods

[CocoaPods](http://cocoapods.org) You can install it with the following command:

```bash
$ gem install cocoapods
```

> CocoaPods 1.1+ is required to build.

Then, run the following command:

```bash
$ pod install
```

Then, select another bundle ID and Team and Run the project on Xcode:

```bash
Let's Rock....
```


License
----
MIT

**Free Software, Hell Yeah!**
