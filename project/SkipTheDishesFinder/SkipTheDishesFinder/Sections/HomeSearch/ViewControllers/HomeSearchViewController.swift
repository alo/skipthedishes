//
//  HomeSearchViewController.swift
//  SkipTheDishesFinder
//
//  Created by Mauricio Olimpio on 24/06/18.
//  Copyright © 2018 Mauricio Olimpio. All rights reserved.
//

import UIKit

import GooglePlaces
import GoogleMaps


class HomeSearchViewController: BaseNavigationViewController {

    
    @IBOutlet weak var seeOnMapButton: UIButton!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    private let locationManager = CLLocationManager()
    var currentLocation : CLLocation?
    
    var presenter:HomeSearchPresenter = HomeSearchPresenter()
    

    var restaurantList:RestaurantList?
    
    
    /// View customizada do Mapa
    lazy var customMap: CustomMapView = {
        let customMap = CustomMapView(frame: CGRect(x: 0, y: self.tableView.frame.origin.y, width: self.view.frame.width, height: self.tableView.frame.height))
        customMap.isHidden = true
        customMap.delegate = self
        return customMap
    }()
    
    /// View Customizada que mostra o endereço escolhido na controller do Auto Complete
    lazy var customAutoCompleteView : CustomAutoCompleteSearchView = {
        let customAutoCompleteView = CustomAutoCompleteSearchView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 120))
        customAutoCompleteView.delegate = self
        customAutoCompleteView.isHidden = true
        return customAutoCompleteView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleText = "MAIS_PROXIMOS".localized
        self.searchButton.layer.zPosition = 10
        self.searchButton?.shadowWith(color: UIColor.black.cgColor, opacity: 0.3, size: CGSize.zero, radius: 2.0)
        
        self.presenter.delegate = self
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
    }
    override func viewDidAppear(_ animated: Bool) {
        self.registerCells()
        initCustomViews()
        
        ProgressHud.shared.show()
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
    }
    
    /// Insere views customizadas na tela
    func initCustomViews(){
        self.view.addSubview(customMap)
        self.view.addSubview(customAutoCompleteView)
    }
    
   
    
    func registerCells() {
        self.tableView.register(UINib(nibName: "SearchRestaurantsTableViewCell", bundle: nil), forCellReuseIdentifier: "SearchRestaurantCell")
    }
    
    /// Mostra a controller dos endereços com animação
    ///
    /// - Parameter direction: Direção da animação da controller
    func showSearchControllerAnimation(direction: String){
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionPush
        transition.subtype = direction
        transition.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
    }
    
    /// Action que mostra a Custom View do Mapa, com a respectiva localização
    ///
    /// - Parameter sender: ""
    @IBAction func seeOnMap_action(sender: Any){
        let texto = (customMap.isHidden) ? "VER_EM_LISTA".localized : "VER_NO_MAPA".localized
        seeOnMapButton.setTitle(texto, for: .normal)
        (sender as! UIButton).setTitle(texto, for: .normal)
        if let _location = currentLocation{
            customMap.setFocusOnRestaurantMark(
                lat:_location.coordinate.latitude,
                lng: _location.coordinate.longitude)
        }
        customMap.isHidden = !(customMap.isHidden)
        customMap.showAllMarkers()
    }
   
    @IBAction func search_action(){
        let searchViewController = self.storyboard?.instantiateViewController(withIdentifier: "AutoCompleteSearchViewController") as! AutoCompleteSearchViewController
        searchViewController.delegate = self
        searchViewController.modalPresentationStyle = .overCurrentContext
        searchViewController.currentLocation = locationManager.location
        showSearchControllerAnimation(direction: kCATransitionFromRight)
        present(searchViewController, animated: false, completion: nil)
    }
}

extension HomeSearchViewController:HomeSearchPresenterProtocol{
    
    func returnGetRestaurantsByCoordinates(success: Bool, restaurantList: RestaurantList?) {
        
        if success {
            if let list = restaurantList {
                self.restaurantList = list
                
                self.tableView.reloadData()
                if let _currentLocation = currentLocation{
                    customMap.setMapLocationAndMarkers(restaurants: (restaurantList?.restaurants)!,location: _currentLocation)
                }
            }
        } else {
            self.showAlert(title: AlertTitle.atencao, description: AlertMessage.erroGetRestaurants, buttonTitle: "OK", delegate: nil)
        }
    }
    
}

// MARK: - Delegate das ações correspondentes a Custom View Autocomplete
extension HomeSearchViewController: CustomAutoCompleteSearchProtocol{
    
    func seeOnMapClicked(sender: Any) {
        self.seeOnMap_action(sender: sender)
    }
    
    func filterClicked() {
        rightNavBarButtonPressed()
    }
    
    func closeClicked(){
        customAutoCompleteView.isHidden = true
        self.searchButton.isHidden = false
        self.navigationController?.navigationBar.layer.zPosition = 0
        self.navigationController?.navigationBar.isUserInteractionEnabled = true
        self.currentLocation = nil
        locationManager.startUpdatingLocation()
        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    func backToMapClicked() {
        customAutoCompleteView.isHidden = true
        self.searchButton.isHidden = false
        self.navigationController?.navigationBar.layer.zPosition = 0
        self.navigationController?.navigationBar.isUserInteractionEnabled = true
        let searchViewController = self.storyboard?.instantiateViewController(withIdentifier: "AutoCompleteSearchViewController") as! AutoCompleteSearchViewController
        searchViewController.delegate = self
        searchViewController.modalPresentationStyle = .overCurrentContext
        showSearchControllerAnimation(direction: kCATransitionFromLeft)
        present(searchViewController, animated: false, completion: nil)
     
        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
    }
}

extension HomeSearchViewController:UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchRestaurantCell") as! SearchRestaurantsTableViewCell
        cell.delegate = self
        
        if let restaurant = self.restaurantList?.restaurants?[indexPath.row] {
            cell.configure(restaurant: restaurant)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let count = self.restaurantList?.restaurants?.count{
            return count
        }else{
            return 0
        }
        
    }
}

// MARK: - Delegate das ações correspondentes a Controller Auto Complete
extension HomeSearchViewController : AutoCompleteSearchViewControllerProtocol{
    func autoCompleteAddress(city: String, state: String, coordinates: CLLocationCoordinate2D) {
        customAutoCompleteView.isHidden = false
        self.searchButton.isHidden = true
        self.tableView.contentInset = UIEdgeInsetsMake(50, 0, 0, 0)
        customAutoCompleteView.layer.zPosition = 30
        self.navigationController?.navigationBar.layer.zPosition = -1
        self.navigationController?.navigationBar.isUserInteractionEnabled = false
        customAutoCompleteView.city = city
        customAutoCompleteView.state = state
        currentLocation = CLLocation(latitude: coordinates.latitude, longitude: coordinates.longitude)
        self.presenter.getRestaurantsByCoordinate(lat: "\(coordinates.latitude)", lng: "\(coordinates.longitude)")
    }
}

extension HomeSearchViewController:RestaurantDetailProtocol{
    
    func seeOnMapButtonPressed(sender: Any, restaurant: Restaurant) {
        self.seeOnMap_action(sender: sender)
        if let _latitude = restaurant.lat,let _longitude = restaurant.lng{
            customMap.setFocusOnRestaurantMark(lat: _latitude, lng: _longitude)
        }
    }
    
    
    func callButtonPressed(restaurant: Restaurant) {
       /* if let phones = restaurant.phones{
            let viewController = PhoneCallDialogViewController()
            viewController.phones = phones
            self.addChildViewController(viewController)
            self.view.addSubview(viewController.view)
            self.view.bringSubview(toFront: viewController.view)
            
        }else{
            showAlert(title: AlertTitle.atencao, description: AlertMessage.postoSemTelefones, buttonTitle: "OK", delegate: nil)
        }*/
    }
    
    func routeButtonPressed(restaurant: Restaurant) {
        let customMapAlert = CustomMapAlert()
        if let _location = currentLocation{
            customMapAlert.createAndShowDialog(customMap: self.customMap, location: _location, context: self, restaurant: restaurant)
        }else{
            showAlert(title: AlertTitle.atencao, description: AlertMessage.getLocalizacaoFalhou, buttonTitle: "OK", delegate: nil)
        }
    }
}

// MARK: - Localização
extension HomeSearchViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Swift.Error){
        ProgressHud.shared.hide()
        showAlert(title: AlertTitle.atencao, description: AlertMessage.getLocalizacaoFalhou, buttonTitle: "OK", delegate: nil)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        guard status == .authorizedWhenInUse else {
            return
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didStartMonitoringFor region: CLRegion) {
        
        ProgressHud.shared.show()
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let location = locations.first else {
            return
        }
        
        locationManager.stopUpdatingLocation()
        
        if(currentLocation == nil){
            ProgressHud.shared.hide()
            currentLocation = location
            
            
            self.presenter.getRestaurantsByCoordinate(lat: String(location.coordinate.latitude), lng: String(location.coordinate.longitude))
            
        }
        
        
    }
}

