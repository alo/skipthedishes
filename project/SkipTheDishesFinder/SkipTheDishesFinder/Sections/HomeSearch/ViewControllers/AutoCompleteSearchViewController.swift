//
//  AutoCompleteSearchStationViewController.swift
//  AbasteceAi
//
//  Created by João  Pedro on 29/05/18.
//  Copyright © 2018 iTerative. All rights reserved.
//

import Foundation
import UIKit
import GooglePlaces
import Lottie
/// Delegate que controla ação referente ao endereço escolhido
protocol AutoCompleteSearchViewControllerProtocol {
    func autoCompleteAddress(city :String,state : String,coordinates : CLLocationCoordinate2D)
}

class AutoCompleteSearchViewController: UIViewController {
    
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var backButton : UIButton!
    @IBOutlet weak var tableView : UITableView!
    var delegate : AutoCompleteSearchViewControllerProtocol?
    var searchResults = [RestaurantAutoCompleteModel]()
    var googlePlaces = GMSPlacesClient()
    var placesTimer = Timer()
    var placeDistanceTimer = Timer()
    var seconds = 2
    var querySearched = ""
    var currentLocation : CLLocation?

    override func viewDidLoad() {
        super.viewDidLoad()
        backButton.imageView?.contentMode = .scaleAspectFit;
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = CGFloat(AutoCompleteSearchCell.heightForRow)
        self.registerCells()
       searchTextField.addTarget(self, action: #selector(updateDistance), for: .editingChanged)
        runTimer()
    }
    
    deinit {
        placesTimer.invalidate()
        placeDistanceTimer.invalidate()
    }
    
    /// Timer que roda solicitações no Google Places
    func runTimer() {
        placesTimer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(updatePlaces)), userInfo: nil, repeats: true)
    }
    
    @IBAction func backButton_action(){
        dismissControllerWithAnimation(direction: kCATransitionFromLeft)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        placesTimer.invalidate()
    }
    
    /// Método que fecha View Controller com animação
    ///
    /// - Parameter direction: Direção da animação
    func dismissControllerWithAnimation(direction : String){
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionPush
        transition.subtype = direction
        transition.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        dismiss(animated: true, completion: nil)
    }
    
    @objc func updateDistance(){
        placeDistanceTimer.invalidate()
        placeDistanceTimer = Timer.scheduledTimer(
            timeInterval: 5.0,
            target: self,
            selector: #selector(self.getDistance),
            userInfo: ["textField": searchTextField],
            repeats: false)
    }
    
    @objc func getDistance(timer: Timer) {
        for searchPlace in searchResults {
            googlePlaces.lookUpPlaceID(searchPlace.placeId!) { (place, error) in
                if error == nil {
                    if let _location = self.currentLocation{
                       let latitude = CLLocationDegrees((place?.coordinate.latitude)!)
                       let longitude =  CLLocationDegrees((place?.coordinate.longitude)!)
                       let location = CLLocation(latitude: latitude, longitude: longitude)
                       
                    }
                }
            }
        }
        tableView.reloadData()
    }
    
    /// Método que busca endereços no google places e preenche tableView
    @objc func updatePlaces(){
        if(searchTextField.text?.isEmpty)!{
            searchResults.removeAll()
        }else{
            let text = searchTextField.text!
            if(!querySearched.elementsEqual(text) || querySearched.isEmpty){
                googlePlaces.autocompleteQuery(text, bounds: nil, filter: nil) { (results, error) in
                    self.searchResults.removeAll()
                    for index in 0 ..< self.tableView.numberOfRows(inSection: 0){
                        let indexPath = IndexPath(row: index, section: 0)
                        let cell = self.tableView.cellForRow(at: indexPath) as! AutoCompleteSearchCell
                        cell.distanceButton.setImage(UIImage(), for: .normal)
                        cell.distanceButton.setTitle("",for: .normal)
                    }
                    self.querySearched = text
                    if results != nil {
                        for result in results!{
                            if let result = result as? GMSAutocompletePrediction{
                                self.searchResults.append(RestaurantAutoCompleteModel(place: result))
                            }
                        }
                    }
                }
            }
        }
        self.tableView.reloadData()
    }
    
    /// Limpa textField
    @IBAction func cleanTextField_action(){
        searchTextField.text = ""
    }
    
    func registerCells() {
        self.tableView.register(UINib(nibName: "AutoCompleteSearchCell", bundle: nil), forCellReuseIdentifier: "autoCompleteSearchCell")
    }
}

extension AutoCompleteSearchViewController: UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "autoCompleteSearchCell") as! AutoCompleteSearchCell
        cell.streetLabel.text = self.searchResults[indexPath.row].street
        cell.provinceLabel.text = self.searchResults[indexPath.row].province
        if let _distance = self.searchResults[indexPath.row].distance{
            cell.removeLoadAndSetDistance(distance: _distance)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.searchResults.count
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        googlePlaces.lookUpPlaceID(self.searchResults[indexPath.row].placeId!) { (place, error) in
                if error == nil {
                    if let _rua = self.searchResults[indexPath.row].street,let _estado = self.searchResults[indexPath.row].province{
                        self.delegate?.autoCompleteAddress(city:  _rua, state: _estado, coordinates: (place?.coordinate)!)
                    }
                }
            self.dismissControllerWithAnimation(direction: kCATransitionFromRight)
        }
    }
}
