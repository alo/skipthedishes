//
//  SearchStationsTableViewCell.swift
//  AbasteceAi
//
//  Created by João  Pedro on 24/05/18.
//  Copyright © 2018 iTerative. All rights reserved.
//

import UIKit

class SearchRestaurantsTableViewCell: UITableViewCell {
    
    var delegate : RestaurantDetailProtocol?
    static let heightForRow = 234
    
    var restaurant : Restaurant?
    @IBOutlet weak var background: UIView!
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var seeOnMapButton : UIButton!
    @IBOutlet weak var addressLabel : UILabel!
    @IBOutlet weak var ratingLabel : UILabel!
   
    let padding : CGFloat = 8.0
    var stackViewTotal: CGFloat = 0.0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.background.shadowWith(color: UIColor.black.cgColor, opacity: 0.1, size: CGSize(width: 0.0, height: 3.0), radius: 5.0)
    }
    
    /// Configura células da tableView
    ///
    /// - Parameter restaurant:
    func configure(restaurant: Restaurant){
        self.restaurant = restaurant
        self.nameLabel.text = restaurant.name
        self.addressLabel.text = restaurant.address
        if let ratin = restaurant.rating{
            self.ratingLabel.text = String(describing: ratin)
        }
        
    }
    
   
    
   
    
    @IBAction func callButon_action(sender: Any){
        self.delegate?.callButtonPressed(restaurant: restaurant!)
    }
    
    @IBAction func routeButton_action(sender: Any){
        self.delegate?.routeButtonPressed(restaurant: restaurant!)
    }
    
    @IBAction func seeOnMap_action(sender: Any){
        self.delegate?.seeOnMapButtonPressed?(sender: sender,restaurant: restaurant!)
    }
    
}


