//
//  AutoCompleteSearchCellTableViewCell.swift
//  AbasteceAi
//
//  Created by João  Pedro on 29/05/18.
//  Copyright © 2018 iTerative. All rights reserved.
//

import UIKit
import Lottie

class AutoCompleteSearchCell: UITableViewCell {
    
    @IBOutlet weak var streetLabel : UILabel!
    @IBOutlet weak var distanceButton : UIButton!
    static let heightForRow = 70.0
    @IBOutlet weak var provinceLabel: UILabel!
    private let animationView = LOTAnimationView(name: "LottieAnimation")
    private let animationViewTag = 666
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
            let colorCallback = LOTColorValueCallback(color: ConstantsColors.SkipPrimaryColor.cgColor)
            let keypath = LOTKeypath(string: "**.Stroke 1.Color")
            animationView.setValueDelegate(colorCallback, for: keypath)
            animationView.tag = animationViewTag
            animationView.contentMode = .scaleAspectFill
            animationView.loopAnimation = true
            animationView.frame = CGRect(x: 0.0, y: 0.0, width: 50.0, height: 50.0);
            animationView.center = self.distanceButton.convert(distanceButton.center, from:distanceButton.superview)
            distanceButton.addSubview(animationView)
            animationView.play()
    }
    
    func removeLoadAndSetDistance(distance: String){
        DispatchQueue.main.async {
            self.distanceButton.viewWithTag(self.animationViewTag)?.removeFromSuperview()
            self.distanceButton.setImage(UIImage(named: "icon_pin"), for: .normal)
            self.distanceButton.setTitle(distance, for: .normal)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
