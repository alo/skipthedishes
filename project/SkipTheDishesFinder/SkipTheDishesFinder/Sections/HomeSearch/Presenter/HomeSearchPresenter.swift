//
//  HomeSearchPresente.swift
//  SkipTheDishesFinder
//
//  Created by Mauricio Olimpio on 24/06/18.
//  Copyright © 2018 Mauricio Olimpio. All rights reserved.
//

import UIKit

@objc protocol RestaurantDetailProtocol {
    
    func callButtonPressed(restaurant: Restaurant)
    func routeButtonPressed(restaurant: Restaurant)
    @objc optional func seeOnMapButtonPressed(sender:Any,restaurant: Restaurant)
}


protocol HomeSearchPresenterProtocol {
 
    func returnGetRestaurantsByCoordinates(success: Bool, restaurantList: RestaurantList?)
    
}

class HomeSearchPresenter {

    var delegate:HomeSearchPresenterProtocol?
    var googlePlaces:GooglePlacesDomain
    
    init() {
        self.googlePlaces = GooglePlacesDomain()
    }
    
    func getRestaurantsByCoordinate(lat: String,lng:String) {
        self.googlePlaces.getRestaurantsListByCoordinates(lat: lat, lng: lng) { (success, restaurantList) in
            self.delegate?.returnGetRestaurantsByCoordinates(success: success, restaurantList: restaurantList)
        }
    }
    
}
