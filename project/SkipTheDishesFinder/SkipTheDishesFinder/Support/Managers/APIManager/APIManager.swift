//
//  APIManager.swift
//  PocMVP
//
//  Created by Matheus Alves on 20/12/17.
//  Copyright © 2017 Iterative. All rights reserved.
//

import Foundation
import Alamofire

class APIManager {
    var header: HTTPHeaders?
    
    init(contentType: String = "application/json") {
        
        header = [
            "Content-Type" : contentType
           
        ]
    }
    
    func get(url: String, headers: HTTPHeaders?, completion: @escaping (Data?, Int) -> Void) {
        let request = Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
        
        request.responseJSON { response in
            var result = ""
            if response.result.value == nil || response.result.value is NSNull {
                result = ""
            } else {
                result = try! (String(data: JSONSerialization.data(withJSONObject: response.result.value ?? "", options: .prettyPrinted), encoding: .utf8) ?? "").replacingOccurrences(of: "\n", with: "")
            }

            completion(result.data(using: .utf8), response.response?.statusCode ?? 0)
        }
        
        
    }
    
    func post(url: String, parameters: [String: Any]?, headers: HTTPHeaders?, completion: @escaping (Data?, Int) -> Void) {
        let request = Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
        
        request.responseJSON { response in
            var result = ""
            if response.result.value == nil || response.result.value is NSNull {
                result = ""
            } else {
                result = try! (String(data: JSONSerialization.data(withJSONObject: response.result.value ?? "", options: .prettyPrinted), encoding: .utf8) ?? "").replacingOccurrences(of: "\n", with: "")
            }
            completion(result.data(using: .utf8), response.response?.statusCode ?? 0)
        }
        
        
    }
    
    func postUrlEncoded(url: String, parameters: [String: Any]?, headers: HTTPHeaders?, completion: @escaping (Data?, Int) -> Void) {
        let request = Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding(), headers: headers)
        
        request.responseJSON { response in
            var result = ""
            if response.result.value == nil || response.result.value is NSNull {
                result = ""
            } else {
                result = try! (String(data: JSONSerialization.data(withJSONObject: response.result.value ?? "", options: .prettyPrinted), encoding: .utf8) ?? "").replacingOccurrences(of: "\n", with: "")
            }
            completion(result.data(using: .utf8), response.response?.statusCode ?? 0)
        }
        
        
    }
    
    func postWithoutJson(url: String, parameters: [String: Any]?, headers: HTTPHeaders?, completion: @escaping (Data?, Int) -> Void) {
        let request = Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
        
        request.responseJSON { response in
            var result = ""
            if response.result.value == nil || response.result.value is NSNull {
                result = ""
            } else {
                result = "OK"
            }
            completion(result.data(using: .utf8), response.response?.statusCode ?? 0)
        }
        
        
    }
    
    func put(url: String, parameters: [String: Any]?, headers: HTTPHeaders?, completion: @escaping (Data?, Int) -> Void) {
        print(parameters!)
        let request = Alamofire.request(url, method: .put, parameters: parameters!, encoding: JSONEncoding.default, headers: headers)
        
        request.responseJSON { response in
            var result = ""
            
            if response.result.value == nil || response.result.value is NSNull {
                result = ""
            } else {
                result = try! (String(data: JSONSerialization.data(withJSONObject: response.result.value ?? "", options: .prettyPrinted), encoding: .utf8) ?? "").replacingOccurrences(of: "\n", with: "")
            }
            completion(result.data(using: .utf8), response.response?.statusCode ?? 0)
        }
        
       
    }
    
    
    func delete(url: String, parameters: [String: Any]?, headers: HTTPHeaders?, completion: @escaping (Data?, Int) -> Void) {
        let request = Alamofire.request(url, method: .delete, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
        
        request.responseJSON { response in
            var result = ""
            if response.result.value == nil || response.result.value is NSNull {
                result = ""
            } else {
                result = try! (String(data: JSONSerialization.data(withJSONObject: response.result.value ?? "", options: .prettyPrinted), encoding: .utf8) ?? "").replacingOccurrences(of: "\n", with: "")
            }
            completion(result.data(using: .utf8), response.response?.statusCode ?? 0)
        }
        
        
    }
    
   
}
