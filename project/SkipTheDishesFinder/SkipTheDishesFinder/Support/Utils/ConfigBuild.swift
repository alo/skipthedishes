//
//  ConfigBuild.swift
//  SkipTheDishesFinder
//
//  Created by Mauricio Olimpio on 25/06/18.
//  Copyright © 2018 Mauricio Olimpio. All rights reserved.
//

import UIKit

class ConfigBuildUtils {
    static let configBuildDict = Bundle.main.object(forInfoDictionaryKey: "ConfigBuild") as! [String:Any]
    
    //GOOGLE API KEY
    static let googleKey = ConfigBuildUtils.configBuildDict["GOOGLE_KEY"] as! String
}
