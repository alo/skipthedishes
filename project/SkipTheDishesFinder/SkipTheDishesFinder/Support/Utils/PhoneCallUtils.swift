//
//  PhoneCall.swift
//  AbasteceAi
//
//  Created by João  Pedro on 25/05/18.
//  Copyright © 2018 iTerative. All rights reserved.
//

import Foundation
import UIKit

class PhoneCallUtils{
    static func formatNumber(number :inout String) -> String{
        number.insert("(", at: number.startIndex)
        number.insert(")", at: number.index(number.startIndex, offsetBy: 4))
        number.insert(" ", at: number.index(number.startIndex, offsetBy: 5))
        var espaco = 0
        if(number.components(separatedBy: ") ")[0].count == 8){
             espaco = 9
        }else{
            espaco = 10
        }
         number.insert("-", at: number.index(number.startIndex, offsetBy: espaco))
        return number
    }
}

extension String {
    
    enum RegularExpressions: String {
        case phone = "^\\s*(?:\\+?(\\d{1,3}))?([-. (]*(\\d{3})[-. )]*)?((\\d{3})[-. ]*(\\d{2,4})(?:[-.x ]*(\\d+))?)\\s*$"
    }
    
    func isValid(regex: RegularExpressions) -> Bool {
        return isValid(regex: regex.rawValue)
    }
    
    func isValid(regex: String) -> Bool {
        let matches = range(of: regex, options: .regularExpression)
        return matches != nil
    }
    
    func onlyDigits() -> String {
        let filtredUnicodeScalars = unicodeScalars.filter{CharacterSet.decimalDigits.contains($0)}
        return String(String.UnicodeScalarView(filtredUnicodeScalars))
    }
    
    func makeACall() {
        if isValid(regex: .phone) {
            if let url = URL(string: "tel://\(self.onlyDigits())"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
}
