//
//  BaseNavigationViewController.swift
//  AbasteceAi
//
//  Created by Matheus Alves on 16/02/18.
//  Copyright © 2018 Matheus Alves. All rights reserved.
//

import UIKit
import QuartzCore


class BaseNavigationViewController: BaseViewController {
    
    let titleLabel = UILabel()
    var titleText: String? {
        didSet {
            self.changeNavigationBarTitle(title: self.titleText ?? "")
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.barTintColor = .white
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.backBarButtonItem?.tintColor = UIColor(red: 4/255, green: 103/255, blue: 162/255, alpha: 1)

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.resignFirstResponder()
    }
    
    func setRightNavbarButton(with image: UIImage){

        let backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        backButton.setImage(image, for: .normal)
        backButton.imageView?.contentMode = .scaleAspectFit
        backButton.addTarget(self, action: #selector(rightNavBarButtonPressed), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    func hideViews(){
        self.titleLabel.isHidden = true
        if let _ = self.navigationItem.rightBarButtonItems{
           // rightItens.map{$0.customView?.isHidden = true}
            for item in self.navigationItem.rightBarButtonItems! {
                item.customView?.isHidden = true
            }
        }
        if let _ = self.navigationItem.leftBarButtonItems{
            // rightItens.map{$0.customView?.isHidden = true}
            for item in self.navigationItem.leftBarButtonItems! {
                item.customView?.isHidden = true
            }
        }
        self.navigationItem.setHidesBackButton(true, animated: false)
    }
    
    func showViews(){
       self.titleLabel.isHidden = false
        if let _ = self.navigationItem.rightBarButtonItems{
            // rightItens.map{$0.customView?.isHidden = true}
            for item in self.navigationItem.rightBarButtonItems! {
                item.customView?.isHidden = false
            }
        }
        if let _ = self.navigationItem.leftBarButtonItems{
            // rightItens.map{$0.customView?.isHidden = true}
            for item in self.navigationItem.leftBarButtonItems! {
                item.customView?.isHidden = false
            }
        }
        self.navigationItem.setHidesBackButton(false, animated: false)
    }
    
    @objc func rightNavBarButtonPressed(){
        fatalError("Método não implementado na classe filha!")
    }
    
    func changeNavigationBarTitle(title: String) {
        titleLabel.text = title
        titleLabel.textColor = UIColor(red: 4/255, green: 103/255, blue: 162/255, alpha: 1)
        titleLabel.font = UIFont(name: "WorkSans-Bold", size: 25)
        titleLabel.sizeToFit()
        titleLabel.frame.origin = CGPoint(x: titleLabel.frame.origin.x, y: titleLabel.frame.origin.y + 10)
        
        let leftItem = UIBarButtonItem(customView: titleLabel)
        
        self.navigationItem.leftItemsSupplementBackButton = true
        self.navigationItem.leftBarButtonItem = leftItem
    }
}
