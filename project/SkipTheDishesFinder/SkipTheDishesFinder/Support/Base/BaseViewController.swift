//
//  BaseViewController.swift
//  Abastece Aí
//
//  Created by Matheus Alves on 27/11/17.
//  Copyright © 2017 Matheus Alves. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func showAlert(title: String, description: String, buttonTitle: String, delegate: CustomAlertProtocol?) {
        let viewController = CustomAlertViewController()
        viewController._title = title
        viewController._description = description
        viewController._buttonTitle = buttonTitle
        viewController.delegate = delegate
        viewController.modalPresentationStyle = .currentContext
        self.addChildViewController(viewController)
        self.view.addSubview(viewController.view)
        self.view.bringSubview(toFront: viewController.view)
    }
    
    func showQuestionAlert(title: String, description: String, positiveButtonTitle: String, negativeButtonTitle: String, delegate: CustomQuestionAlertProtocol, bundle: Any) {
        let viewController = CustomQuestionAlertViewController()
        viewController._title = title
        viewController._description = description
        viewController._positiveButtonTitle = positiveButtonTitle
        viewController._negativeButtonTitle = negativeButtonTitle
        viewController.bundle = bundle
        viewController.delegate = delegate
        viewController.modalPresentationStyle = .currentContext
        self.addChildViewController(viewController)
        self.view.addSubview(viewController.view)
        self.view.bringSubview(toFront: viewController.view)
    }
    

    
    func showPositiveAlert(title: String, description: String, positiveButtonTitle: String, delegate: CustomQuestionAlertProtocol) {
        let viewController = CustomQuestionAlertViewController()
        viewController._title = title
        viewController._description = description
        viewController._positiveButtonTitle = positiveButtonTitle
        viewController.delegate = delegate
        viewController.modalPresentationStyle = .currentContext
        self.addChildViewController(viewController)
        self.view.addSubview(viewController.view)
        self.view.bringSubview(toFront: viewController.view)
    }
}
