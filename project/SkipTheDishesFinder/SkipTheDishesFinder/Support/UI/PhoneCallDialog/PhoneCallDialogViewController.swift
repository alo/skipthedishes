//
//  PhoneCallDialogViewController.swift
//  AbasteceAi
//
//  Created by João  Pedro on 28/05/18.
//  Copyright © 2018 iTerative. All rights reserved.
//

import UIKit
import QuartzCore

class PhoneCallDialogViewController: UIViewController {
    
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleLabel : UILabel?    
    var phones : [String]!
    var formattedPhones = [String]()
    var phoneCount = 0
    var parentController : BaseNavigationViewController?
    let bg = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        guard (phones != nil)  else {
            fatalError("Não há telefones")
        }
        phoneCount = (phones?.count)!
        formattedPhones = phones
        for i in 0..<formattedPhones.count {
            formattedPhones.append(PhoneCallUtils.formatNumber(number : &formattedPhones[i]))
        }
        self.configureViews()
    }
    
    @IBAction func cancelar_Action() {
        if let _parentController = parentController{
            _parentController.showViews()
        }
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
        bg.removeFromSuperview()
    }
    
    func configureViews() {
       self.button.layer.cornerRadius = 5
        self.view.layer.cornerRadius = 10
        self.view.layer.masksToBounds = true
        var viewHeight: CGFloat = 0.0
        if(phoneCount > 8){
            viewHeight = UIScreen.main.bounds.height / 2
        }else{
            tableView.layoutIfNeeded()
            viewHeight = tableView.contentSize.height + (titleLabel?.layer.frame.height)! + button.layer.frame.height + 40
        }
        self.view.frame.size = CGSize(width: UIScreen.main.bounds.width - 40, height: viewHeight)
        self.view.center = self.parent!.view.center
        bg.backgroundColor = .black
        bg.alpha = 0.7
        bg.frame = UIScreen.main.bounds
        bg.layer.zPosition = 100
        self.view.layer.zPosition = 101
        if(self.parent!.isKind(of: BaseNavigationViewController.self)){
            parentController = self.parent as? BaseNavigationViewController
            parentController?.hideViews()
        }
        self.parent!.view.addSubview(bg)
        
    }
    
}
extension PhoneCallDialogViewController : UITableViewDataSource,UITableViewDelegate{
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return phoneCount
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = UITableViewCell()
        cell.textLabel?.text = formattedPhones[indexPath.row]
        cell.textLabel?.font = UIFont (name: "WorkSans-Medium", size: 13)
        cell.textLabel?.textColor = UIColor.gray
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        phones![indexPath.row].makeACall()
    }
}

