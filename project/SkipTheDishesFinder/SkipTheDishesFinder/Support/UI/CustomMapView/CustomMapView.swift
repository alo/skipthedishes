//
//  CustomMapView.swift
//  AbasteceAi
//
//  Created by João  Pedro on 28/05/18.
//  Copyright © 2018 iTerative. All rights reserved.
//

import UIKit
import GoogleMaps

class CustomMapView: UIView {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var mapView : GMSMapView?
    @IBOutlet weak var detailView : UIView?
    @IBOutlet weak var detailNameLabel : UILabel?
    @IBOutlet weak var detailDistanceLabel: UILabel?
    @IBOutlet weak var detailAddressLabel : UILabel?
    @IBOutlet weak var detailFavoriteButton : UIButton?
    @IBOutlet weak var detailCallButton : UIButton?
    @IBOutlet weak var detailRouteButton : UIButton?
    @IBOutlet weak var myLocationButton : UIButton?
    var routePolylines = GMSPolyline()
    var restaurants = [Restaurant]()
    var servicesInMarkCount = 0
    var myCurrentLocation : CLLocation?
    var markers = [GMSMarker]()
    var selectedRestaurant : Restaurant?
    var delegate:RestaurantDetailProtocol?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    func setup() {
        contentView = loadViewFromNib()
        contentView.frame = bounds
        contentView.autoresizingMask = [UIViewAutoresizing.flexibleWidth,
                                        UIViewAutoresizing.flexibleHeight]
        addSubview(contentView)
        self.detailView?.isHidden = true
        self.mapView?.delegate = self

    }
    
    func loadViewFromNib() -> UIView! {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    
    
    func setMapLocationAndMarkers(restaurants : [Restaurant],location : CLLocation){
        self.mapView?.clear()
        self.myCurrentLocation = location
        self.restaurants = restaurants
        self.mapView?.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
        let position = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let myPosition = GMSMarker(position: position)
        myPosition.icon = GMSMarker.markerImage(with: ConstantsColors.SkipPrimaryColor)
        for restaurant in self.restaurants {
            if let _lat = restaurant.lat, let _lng = restaurant.lng {
                let position = CLLocationCoordinate2D(latitude: _lat, longitude: _lng)
                let mark = GMSMarker(position: position)
                mark.icon = UIImage(named: "icon_pin")
                mark.title = restaurant.name!
                mark.map = mapView
                markers.append(mark)
            }
        }
    }
    
    func setFocusOnRestaurantMark(lat:Double,lng : Double){
        let location = CLLocationCoordinate2D(latitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(lng))
        self.mapView?.animate(toLocation: location)
        self.mapView?.animate(toZoom: 15)
        let marker = markers.filter{$0.position.latitude == location.latitude && $0.position.longitude == location.longitude}
        if let _marker = marker.first  {
             hightlightSelectedMarker(marker: _marker)
        }
    }
    
    func hightlightSelectedMarker(marker: GMSMarker){
        let _ = markers.map { (_marker) -> GMSMarker in
            if(_marker == marker){
                _marker.icon = UIImage(named: "icon_pin")
            }else{
                _marker.icon = UIImage(named: "icon_pin_large")
            }
            return _marker
        }
    }
    
    func configureAndShowDetail(restaurant: Restaurant){
        
        detailNameLabel?.text = restaurant.name
        detailAddressLabel?.text = restaurant.address
        
        if((self.detailView?.isHidden)!){
            UIView.animate(withDuration: 0.3) {
                self.detailView?.isHidden = false
            }
        }
    }
    
    func showAllMarkers(){
        DispatchQueue.main.async(execute: {
            self.routePolylines.map = nil
        })
        let _ = markers.map { (marker) -> GMSMarker in
            marker.icon = UIImage(named: "icon_pin")
            marker.opacity = 1.0
            return marker
        }
    }
    
    func drawRoute(from source: CLLocation, to destination: Restaurant,context: BaseNavigationViewController){
        let _ = markers.compactMap{$0}.filter{$0.title != destination.name!}.map{$0.opacity = 0.0}
        routePolylines.path = nil
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        if let latitude = destination.lat,let longitude = destination.lng{
            let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(source.coordinate.latitude),\(source.coordinate.longitude)&destination=\(latitude),\(longitude)&sensor=false&mode=driving")!
            
            let task = session.dataTask(with: url, completionHandler: {
                (data, response, error) in
                if error != nil {
                    print(error!.localizedDescription)
                } else {
                    do {
                        if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any] {
                            let preRoutes = json["routes"] as! NSArray
                            
                            if (preRoutes.count > 0){
                                let routes = preRoutes[0] as! NSDictionary
                                let routeOverviewPolyline:NSDictionary = routes.value(forKey: "overview_polyline") as! NSDictionary
                                let polyString = routeOverviewPolyline.object(forKey: "points") as! String
                                DispatchQueue.main.async(execute: {
                                    let path = GMSPath(fromEncodedPath: polyString)
                                    let _bounds = GMSCoordinateBounds(path: path!)
                                    self.mapView?.animate(with: GMSCameraUpdate.fit(_bounds))
                                    self.routePolylines.path = path
                                    self.routePolylines.strokeWidth = 3.0
                                    self.routePolylines.strokeColor = ConstantsColors.SkipPrimaryColor
                                    self.routePolylines.map = self.mapView
                                    
                                })
                            }else{
                                DispatchQueue.main.async(execute: {
                                    self.routePolylines.path = nil
                                })
                                context.showAlert(title: AlertTitle.atencao, description: AlertMessage.erroTracarRota, buttonTitle: "OK", delegate: nil)
                            }
                        }
                        
                    } catch {
                        context.showAlert(title: AlertTitle.atencao, description: AlertMessage.erroTracarRota, buttonTitle: "OK", delegate: nil)
                    }
                }
            })
            task.resume()
        }
    }
    
    @IBAction func closeDetail_action(){
        UIView.animate(withDuration: 0.3) {
            self.detailView?.isHidden = true
        }
    }
    
    @IBAction func centerOnMyPosition_action(){
        if let _location = myCurrentLocation{
            self.mapView?.camera = GMSCameraPosition(target: _location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
        }
    }
    
  
    @IBAction func callButon_action(sender: Any){

        self.delegate?.callButtonPressed(restaurant:  selectedRestaurant!)
    }
    
    @IBAction func routeButton_action(sender: Any){
        self.delegate?.routeButtonPressed(restaurant: selectedRestaurant!)
    }
    
}

extension CustomMapView : GMSMapViewDelegate{

    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        setFocusOnRestaurantMark(lat: marker.position.latitude, lng: marker.position.longitude)
        selectedRestaurant = restaurants.filter({$0.name == marker.title}).first
        if(selectedRestaurant != nil){
            configureAndShowDetail(restaurant: selectedRestaurant!)
        }
        return true
    }
}


