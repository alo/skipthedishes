//
//  AlertsString.swift
//  AbasteceAi
//
//  Created by Jefferson Batista on 6/12/18.
//  Copyright © 2018 iTerative. All rights reserved.
//

import Foundation

struct AlertButton {
    static let sim = "SIM".localized
    static let nao = "NAO".localized
}

struct AlertTitle {
    static let atencao = "ATTENTION_TITLE".localized
    static let sucesso = "SUCCESS_TITLE".localized
}

struct AlertMessage {
    static let desejaExcluirCartao        = "DESEJA_EXCLUIR_CARTAO".localized
    static let cartaoExcluidoSucesso      = "CARTAO_EXCLUIDO_SUCESSO".localized
    static let pagamentoCadastradoSucesso = "PAGAMENTO_CADASTRADO_SUCESSO".localized
    static let erroBuscaCartao            = "ERRO_BUSCA_CARTAO".localized
    static let erroCadastrarCartao        = "ERRO_CADASTRAR_CARTAO".localized
    static let erroExcluirCartao          = "ERRO_EXCLUIR_CARTAO".localized
    
    //STATIONS
    static let postoNaoPodeSerFavoritado    = "FAVORITAR_POSTO_NAO_HABILITADO".localized
    static let postoSemTelefones            = "SEM_TELEFONES".localized
    static let erroGetPostos                = "ERRO_GET_POSTOS".localized
    static let erroGetRestaurants           = "ERRO_GET_RESTAURANTS".localized
    //MAPS
    static let getLocalizacaoFalhou = "GET_LOCALIZACAO_FALHOU".localized
    static let erroTracarRota = "ERRO_TRACAR_ROTA".localized
}
