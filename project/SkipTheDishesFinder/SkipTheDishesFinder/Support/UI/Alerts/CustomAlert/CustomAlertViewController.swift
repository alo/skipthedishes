//
//  CustomAlertViewController.swift
//  AbasteceAi
//
//  Created by Matheus Alves on 27/03/18.
//  Copyright © 2018 Matheus Alves. All rights reserved.
//

import UIKit

protocol CustomAlertProtocol {
    func didReceiveButtonAction()
}

class CustomAlertViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var button: UIButton!
    
    var _title: String = ""
    var _description: String = ""
    var _buttonTitle: String = ""
    var parentController : BaseNavigationViewController?
    
    let bg = UIView()
    
    var delegate: CustomAlertProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureViews()
    }
    
    func configureViews() {
        self.button.layer.cornerRadius = 5
        self.view.layer.cornerRadius = 10
        self.view.layer.masksToBounds = true
        self.titleLabel.text = self._title
        self.descriptionLabel.text = self._description
        self.button.setTitle(self._buttonTitle, for: .normal)
        self.view.frame.size = CGSize(width: UIScreen.main.bounds.width - 40, height: 250)
        self.view.center = self.parent!.view.center
        
        bg.backgroundColor = .black
        bg.alpha = 0.7
        bg.frame = UIScreen.main.bounds
        bg.layer.zPosition = 100
        if(self.parent!.isKind(of: BaseNavigationViewController.self)){
            parentController = self.parent as? BaseNavigationViewController
            parentController?.hideViews()
        }
        self.view.layer.zPosition = 101
        self.parent!.view.addSubview(bg)
    }

    @IBAction func okButton_Action() {
        self.delegate?.didReceiveButtonAction()
        if let _parentController = parentController{
            _parentController.showViews()
        }
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
        bg.removeFromSuperview()
    }

}
