//
//  CustomQuestionAlertViewController.swift
//  AbasteceAi
//
//  Created by Matheus Alves on 29/03/18.
//  Copyright © 2018 Matheus Alves. All rights reserved.
//

import UIKit

protocol CustomQuestionAlertProtocol {
    func didReceiveAnswer(isPositive: Bool, bundle: Any?)
}

class CustomQuestionAlertViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var positiveButton: UIButton!
    @IBOutlet weak var negativeButton: UIButton!
    
    var delegate: CustomQuestionAlertProtocol?
    
    var _title: String = ""
    var _description: String = ""
    var _positiveButtonTitle: String = ""
    var _negativeButtonTitle: String = ""
    var bundle: Any? = nil
    
    let bg = UIView()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureViews()
    }
    
    func configureViews() {
        self.view.layer.cornerRadius = 10
        self.view.layer.masksToBounds = true
        self.positiveButton.layer.cornerRadius = 5
        self.negativeButton.layer.cornerRadius = 5
        self.negativeButton.layer.borderWidth = 1
        self.negativeButton.layer.borderColor = self.positiveButton.backgroundColor?.cgColor
        self.titleLabel.text = self._title
        self.descriptionLabel.text = self._description
        self.positiveButton.setTitle(self._positiveButtonTitle, for: .normal)
        self.negativeButton.setTitle(self._negativeButtonTitle, for: .normal)
        self.view.frame.size = CGSize(width: UIScreen.main.bounds.width - 40, height: 250)
        self.view.center = self.parent!.view.center
        
        bg.backgroundColor = .black
        bg.alpha = 0.7
        bg.frame = UIScreen.main.bounds
        bg.layer.zPosition = 100
        self.view.layer.zPosition = 101
        self.parent!.view.addSubview(bg)
    }
    
    @IBAction func positiveButton_Action() {
        self.delegate?.didReceiveAnswer(isPositive: true, bundle: self.bundle)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
        bg.removeFromSuperview()
    }

    @IBAction func negativeButton_Action() {
        self.delegate?.didReceiveAnswer(isPositive: false, bundle: self.bundle)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
        bg.removeFromSuperview()
    }
}
