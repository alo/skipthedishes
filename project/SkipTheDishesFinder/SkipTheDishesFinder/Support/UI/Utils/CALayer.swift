//
//  CALayer.swift
//  AbasteceAi
//
//  Created by Mauricio Olimpio on 28/11/17.
//  Copyright © 2017 Matheus Alves. All rights reserved.
//

import UIKit
import QuartzCore

extension CALayer {
    
    func setIBShadowColor(color: UIColor) {
        shadowColor = color.cgColor
    }
    
    func setIBBorderColor(color: UIColor) {
        borderColor = color.cgColor
    }
    
}
