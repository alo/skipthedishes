//
//  String.swift
//  AbasteceAi
//
//  Created by Matheus Alves on 07/12/17.
//  Copyright © 2017 Matheus Alves. All rights reserved.
//

import Foundation

extension String {
    
    init(float: Float) {
        self = String(format: "%.2f", float as CVarArg).replacingOccurrences(of: ".", with: ",")
    }
    
    func currencyInputFormatting(more: Double) -> String {
        
        var number: NSNumber!
        let formatter = NumberFormatter()
        formatter.numberStyle = .currencyAccounting
        formatter.currencySymbol = ""
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        
        var amountWithPrefix = self
        
        let regex = try! NSRegularExpression(pattern: "[^0-9]", options: .caseInsensitive)
        amountWithPrefix = regex.stringByReplacingMatches(in: amountWithPrefix, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.count), withTemplate: "")
        
        let double = (amountWithPrefix as NSString).doubleValue
        number = NSNumber(value: (double / 100) + more >= 1000 ? 999.99 : (double / 100) + more)
        
        guard number != 0 as NSNumber else {
            return ""
        }
        
        return formatter.string(from: number)!.replacingOccurrences(of: ".", with: ",")
    }
    
    func removingWhitespaces() -> String {
        return components(separatedBy: .whitespaces).joined()
    }
}

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}
