//
//  UIView.swift
//  AbasteceAi
//
//  Created by Mauricio Olimpio on 06/12/17.
//  Copyright © 2017 Matheus Alves. All rights reserved.
//

import UIKit
import QuartzCore

extension UIView {
    
    func buildShadow(){
        
            let shadowLayer = UIView(frame: self.frame)
            shadowLayer.backgroundColor = UIColor.white
            shadowLayer.layer.shadowColor = self.layer.shadowColor
            shadowLayer.layer.shadowOffset = self.layer.shadowOffset
            shadowLayer.layer.shadowOpacity = self.layer.shadowOpacity
            shadowLayer.layer.shadowRadius = self.layer.shadowRadius
            shadowLayer.layer.cornerRadius = self.layer.cornerRadius
            
            self.superview?.insertSubview(shadowLayer, belowSubview: self)
        
    }
    
    func shadowWith(color: CGColor,opacity: Float,size : CGSize,radius : CGFloat){
        self.layer.shadowColor = color
        self.layer.shadowOpacity = opacity
        self.layer.shadowOffset = size
        self.layer.shadowRadius = radius
    }
    
}

