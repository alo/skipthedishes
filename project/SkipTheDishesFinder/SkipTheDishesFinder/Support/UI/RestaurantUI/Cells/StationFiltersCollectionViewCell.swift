//
//  StationFiltersCollectionViewCell.swift
//  AbasteceAi
//
//  Created by João  Pedro on 24/05/18.
//  Copyright © 2018 iTerative. All rights reserved.
//

import UIKit

class StationFiltersCollectionViewCell: UICollectionViewCell {
    
    let widthForRow = 30
    @IBOutlet weak var filterName : UILabel?
    @IBOutlet weak var filterImage : UIImageView?
    @IBOutlet weak var borderView : UIView?
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        borderView?.layer.cornerRadius = 5
        borderView?.layer.borderWidth = 1
        borderView?.layer.borderColor = UIColor(red:0.59, green:0.59, blue:0.59, alpha:1.0).cgColor
        self.contentView.translatesAutoresizingMaskIntoConstraints = false
        widthConstraint.constant = CGFloat(self.widthForRow)
    }
    
}
