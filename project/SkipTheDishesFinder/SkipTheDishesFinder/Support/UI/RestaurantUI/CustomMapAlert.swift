//
//  CustomMapAlert.swift
//  AbasteceAi
//
//  Created by João  Pedro on 31/05/18.
//  Copyright © 2018 iTerative. All rights reserved.
//
import Foundation
import UIKit
import GoogleMaps
import MapKit

class CustomMapAlert {
    
    func createAndShowDialog(customMap: CustomMapView,location : CLLocation,context: BaseNavigationViewController,restaurant: Restaurant){
        
        let dialog = UIAlertController(title: "", message: "TRACAR_ROTA_MENSAGEM".localized, preferredStyle: .actionSheet)
        let openWithApp = UIAlertAction(title: "TRACAR_ROTA_SKIP_DISHES".localized, style: .default) { (action) in
            customMap.isHidden = false
            customMap.drawRoute(from: location, to: restaurant,context: context)
        }
        
        let appleMapssApp = UIAlertAction(title: "TRACAR_ROTA_APPLE_MAPS".localized, style: .default) { (action) in
            if let _latitude = restaurant.lat, let _longitude = restaurant.lng{
                let coordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees(_latitude), longitude: CLLocationDegrees(_longitude))
                let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate, addressDictionary:nil))
                mapItem.name = restaurant.address
                mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving])
            }
        }
        dialog.addAction(openWithApp)
        dialog.addAction(appleMapssApp)
        let wazeDialog : UIAlertAction?
        if UIApplication.shared.canOpenURL(URL(string: "waze://")!) {
            wazeDialog = UIAlertAction(title: "TRACAR_ROTA_WAZE".localized, style: .default, handler: { (action) in
                let urlStr: String = "waze://?ll=\(location.coordinate.latitude),\(location.coordinate.longitude)&navigate=yes"
                UIApplication.shared.open(URL(string: urlStr)!, options: [:], completionHandler: nil)
            })
            dialog.addAction(wazeDialog!)
        }
        let cancel = UIAlertAction(title: "TRACAR_ROTA_CANCELAR".localized, style: .cancel, handler: nil)
        dialog.addAction(cancel)
        context.present(dialog, animated: true, completion: nil)
    }
}
