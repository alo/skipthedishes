//
//  CustomAutoCompleteSearchView.swift
//  AbasteceAi
//
//  Created by João  Pedro on 29/05/18.
//  Copyright © 2018 iTerative. All rights reserved.
//
import Foundation
import UIKit

protocol CustomAutoCompleteSearchProtocol {
    func seeOnMapClicked(sender: Any)
    func filterClicked()
    func closeClicked()
    func backToMapClicked()
}

class CustomAutoCompleteSearchView: UIView {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var addressLabel : UILabel!
    @IBOutlet weak var stateLabel : UILabel!
    var delegate : CustomAutoCompleteSearchProtocol?
    var city = "" {
        didSet {
            addressLabel.text = city
        }
    }
    var state = ""{
        didSet{
            stateLabel.text = state
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup() {
        contentView = loadViewFromNib()
        contentView.frame = bounds
        contentView.autoresizingMask = [UIViewAutoresizing.flexibleWidth,
                                        UIViewAutoresizing.flexibleHeight]
        addSubview(contentView)
    }
    @IBAction func btClose_action(){
        self.delegate?.closeClicked()
    }
    
    @IBAction func seeOnMap_action(sender: Any){
        self.delegate?.seeOnMapClicked(sender: sender)
    }
    
    @IBAction func filterClicked(){
        self.delegate?.filterClicked()
    }
    
    @IBAction func backToMap_action(){
        self.delegate?.backToMapClicked()
    }
    
    func loadViewFromNib() -> UIView! {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
}
