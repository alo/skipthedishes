//
//  AppDelegate.swift
//  SkipTheDishesFinder
//
//  Created by Mauricio Olimpio on 24/06/18.
//  Copyright © 2018 Mauricio Olimpio. All rights reserved.
//

import UIKit

import GoogleMaps
import GooglePlaces

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        GMSServices.provideAPIKey(ConfigBuildUtils.googleKey)
        GMSPlacesClient.provideAPIKey(ConfigBuildUtils.googleKey)
        
        
        return true
    }

    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        
        let viewController = UIStoryboard.init(name: "HomeSearch", bundle: nil).instantiateInitialViewController() as! HomeSearchViewController
        
        window?.rootViewController = viewController
        window?.makeKeyAndVisible()
        
        return true
    }


}

