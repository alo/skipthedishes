//
//  GooglePlacesDomain.swift
//  SkipTheDishesFinder
//
//  Created by Mauricio Olimpio on 24/06/18.
//  Copyright © 2018 Mauricio Olimpio. All rights reserved.
//

import UIKit




class GooglePlacesDomain: NSObject {
    
    var restaurantList:RestaurantList?
    var apiManager:APIManager?
    
    func getRestaurantsListByCoordinates(lat: String,lng: String,completion: @escaping (Bool, RestaurantList?) -> Void) {
        ProgressHud.shared.show()
        
        print(lat, lng)
    
        if apiManager == nil{
            self.apiManager = APIManager()
        }
        
        let urlForGoogleSearch = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=\(lat),\(lng)&radius=500&types=restaurant&key=\(ConfigBuildUtils.googleKey)"
        
        self.apiManager?.get(url: urlForGoogleSearch, headers: self.apiManager!.header!, completion: { (data, responseCode) in
        
            ProgressHud.shared.hide()
            
            
            if responseCode == 200 {
                do {
                   
                    let restaurantList = try JSONDecoder().decode(RestaurantList.self, from: data!)
                    self.restaurantList = restaurantList
                    return completion(true, restaurantList)

                } catch {
                    print(error)
                    completion(true, self.restaurantList)
                }
            } else{
                completion(false, self.restaurantList)
            }

            
            
            
        })
        
        
        
        
    }

    
}
