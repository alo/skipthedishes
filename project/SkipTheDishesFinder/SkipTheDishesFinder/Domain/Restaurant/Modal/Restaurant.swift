//
//  Restaurant.swift
//  SkipTheDishesFinder
//
//  Created by Mauricio Olimpio on 24/06/18.
//  Copyright © 2018 Mauricio Olimpio. All rights reserved.
//

import UIKit


struct RestaurantGeometry:Codable {
    let location:RestaurantLocation
}

struct RestaurantLocation:Codable {
    let lat, lng:Double
}

class Restaurant: NSObject, Codable {

    
    var name:String?
    var address : String?
    var id : String?
    var rating : Double?
    var geometry:RestaurantGeometry?
    var icon:String?
    
    var lat:Double?{
        return geometry?.location.lat
    }
    var lng:Double?{
        return geometry?.location.lng
    }
    
    init(name:String, address:String, id:String, rating:Double, geometry:RestaurantGeometry, icon:String) {
        
        self.name = name
        self.address = address
        self.id = id
        self.rating = rating
        self.geometry = geometry
        self.icon = icon
        
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: Keys.self)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        address = try values.decodeIfPresent(String.self, forKey: .address)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        rating = try values.decodeIfPresent(Double.self, forKey: .rating)
        geometry = try values.decodeIfPresent(RestaurantGeometry.self, forKey: .geometry)
        icon = try values.decodeIfPresent(String.self, forKey: .icon)
        
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: Keys.self)
        try container.encode(name, forKey: .name)
        try container.encode(address, forKey: .address)
        try container.encode(id, forKey: .id)
        try container.encode(rating, forKey: .rating)
        try container.encode(geometry, forKey: .geometry)
        try container.encode(icon, forKey: .icon)
        
    }
    
    private enum Keys: String, CodingKey {
        
        case name = "name"
        case address = "vicinity"
        case id = "place_id"
        case rating = "rating"
        case geometry = "geometry"
        case icon = "icon"
    }
}
