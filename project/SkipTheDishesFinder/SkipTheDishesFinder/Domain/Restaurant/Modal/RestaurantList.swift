//
//  RestaurantList.swift
//  AbasteceAi
//
//  Created by João  Pedro on 25/05/18.
//  Copyright © 2018 iTerative. All rights reserved.
//

import Foundation

class RestaurantList:NSObject, Codable {
    
    static let shared = RestaurantList()
    
    var restaurants: [Restaurant]?
    
    init(restaurants: [Restaurant]) {
        self.restaurants = restaurants
    }
    
    override init() {}
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: Keys.self)
        restaurants = try values.decode([Restaurant]?.self, forKey: .restaurants)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: Keys.self)
        try container.encode(restaurants, forKey: .restaurants)
    }
    
    private enum Keys: String, CodingKey {
        case restaurants = "results"
    }
    
    
}
