//
//  StationAutoCompleteModel.swift
//  AbasteceAi
//
//  Created by João  Pedro on 08/06/18.
//  Copyright © 2018 iTerative. All rights reserved.
//

import Foundation
import GooglePlaces

class RestaurantAutoCompleteModel {
    var street : String?
    var province : String?
    var distance : String?
    var placeId : String?
    
    init(place : GMSAutocompletePrediction) {
        self.street = place.attributedPrimaryText.string
        self.placeId = place.placeID
        self.province = place.attributedSecondaryText?.string
    }
}
